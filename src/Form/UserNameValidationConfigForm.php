<?php

namespace Drupal\username_validation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The username_validation config form.
 */
class UserNameValidationConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['username_validation.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'username_validation_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('username_validation.config');

    $form['username_validation_rule'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Username conditions'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    ];

    $form['username_validation_rule']['blacklist_char'] = [
      '#type' => 'textarea',
      '#default_value' => $config->get('blacklist_char'),
      '#title' => $this->t('Blacklist Characters/Words'),
      '#description' => '<p>' . $this->t("Comma-separated characters or words that, if included in the username, will fail validation when creating / updating a user.<br> Example: !,@,#,$,%,^,&,*,(,),1,2,3,4,5,6,7,8,9,0,have,has,were,aren't."),
    ];

    $form['username_validation_rule']['disallow_spaces'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Disallow spaces"),
      '#description' => $this->t('Disallow spaces included in the username'),
      '#default_value' => $config->get('disallow_spaces'),
    ];

    $form['username_validation_rule']['min_char'] = [
      '#type' => 'number',
      '#title' => $this->t("Minimum username characters"),
      '#required' => TRUE,
      '#min' => 2,
      '#description' => $this->t("The minimum number of characters the username should contain"),
      '#default_value' => $config->get('min_char'),
    ];

    $form['username_validation_rule']['max_char'] = [
      '#type' => 'number',
      '#title' => $this->t("Maximum username characters"),
      '#required' => TRUE,
      '#min' => 0,
      '#max' => 60,
      '#description' => $this->t("The maximum number of characters the username should contain"),
      '#default_value' => $config->get('max_char'),
    ];

    // @codingStandardsIgnoreStart
    // @todo Disable ajax_validation for now.
    // See https://www.drupal.org/project/username_validation/issues/3442502
    // $form['username_validation_rule']['ajax_validation'] = [
    //   '#type' => 'checkbox',
    //   '#title' => $this->t('Enable AJAX Validation'),
    //   '#description' => $this->t("Adds AJAX validation to the user and user register form."),
    //   '#default_value' => $config->get('ajax_validation'),
    // ];
    // @codingStandardsIgnoreEnd

    $form['username_validation_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Username Configuration'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    ];

    $form['username_validation_config']['user_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Overwrite "Username" label'),
      '#description' => $this->t('This value will be display instead of the "Username" label in the registration form and when editing a profile.'),
      '#size' => 60,
      '#maxlength' => 255,
      '#default_value' => $config->get('user_label'),
    ];

    $form['username_validation_config']['user_desc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Overwrite "Username" description'),
      '#description' => $this->t('This value will be display instead of the "Username" description in the registration form and when editing a profile.'),
      '#size' => 60,
      '#maxlength' => 255,
      '#default_value' => $config->get('user_desc'),
    ];

    $form['username_validation_config']['skip_existing_username'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Skip validation for existing usernames"),
      '#description' => $this->t('Skips validation for existing usernames if they are unchanged.'),
      '#default_value' => $config->get('skip_existing_username'),
    ];

    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#name' => 'reset',
      '#value' => $this->t('Reset Configuration'),
      '#submit' => ['::clearConfiguration'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $min = $form_state->getValue('username_validation_rule')['min_char'];
    $max = $form_state->getValue('username_validation_rule')['max_char'];

    // Validate min is less than max value.
    if ($min > $max) {
      $form_state->setErrorByName('username_validation_rule][min_char', $this->t('Minimum length should not be more than Max length'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('username_validation.config')
      ->set('blacklist_char', $form_state->getValue('username_validation_rule')['blacklist_char'])
      ->set('min_char', $form_state->getValue('username_validation_rule')['min_char'])
      ->set('max_char', $form_state->getValue('username_validation_rule')['max_char'])
      ->set('disallow_spaces', $form_state->getValue('username_validation_rule')['disallow_spaces'])
      // @codingStandardsIgnoreStart
      // @todo Disable ajax_validation for now.
      // See https://www.drupal.org/project/username_validation/issues/3442502
      // ->set('ajax_validation', $form_state->getValue('username_validation_rule')['ajax_validation'])
      // @codingStandardsIgnoreEnd
      ->set('user_label', $form_state->getValue('username_validation_config')['user_label'])
      ->set('user_desc', $form_state->getValue('username_validation_config')['user_desc'])
      ->set('skip_existing_username', $form_state->getValue('username_validation_config')['skip_existing_username'])
      ->save();
  }

}
